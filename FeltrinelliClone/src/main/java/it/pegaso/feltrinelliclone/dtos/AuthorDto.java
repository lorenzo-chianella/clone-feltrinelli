package it.pegaso.feltrinelliclone.dtos;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorDto implements Serializable {
	
	private Long id;
	private String name;
	private Long yearOfBirth;
	private String birthPlace;
	private CountryDto country;

}
