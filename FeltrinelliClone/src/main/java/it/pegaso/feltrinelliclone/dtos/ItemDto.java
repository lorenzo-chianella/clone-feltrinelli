package it.pegaso.feltrinelliclone.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDto implements Serializable {

	private Long id;
	private String title;
	private Date dateOfRelease;
	private String publisher;
	private BigDecimal price;
	private String ean;
	private Long availability;
	private String imageUrl;
	private Boolean isVisible;
	private AuthorDto author;
	private CountryDto country;
	private GenreDto genre;
	private BookDto book;
	private MovieDto movie;
	private CdDto cd;
	
}
