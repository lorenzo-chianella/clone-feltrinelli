package it.pegaso.feltrinelliclone.dtos;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CdDto implements Serializable {

	private Long id;
	private Long tracks;
	private Long duration;
	
}
