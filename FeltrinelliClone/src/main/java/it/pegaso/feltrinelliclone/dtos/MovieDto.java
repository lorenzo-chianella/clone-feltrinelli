package it.pegaso.feltrinelliclone.dtos;

import java.io.Serializable;

import it.pegaso.feltrinelliclone.enumerators.SupportType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieDto implements Serializable {

	private Long id;
	private String synopsis;
	private SupportType support;
	private Long duration;
	
}
