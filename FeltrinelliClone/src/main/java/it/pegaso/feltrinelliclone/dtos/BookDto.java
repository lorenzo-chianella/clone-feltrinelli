package it.pegaso.feltrinelliclone.dtos;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookDto implements Serializable {

	private Long id;
	private String synopsis;
	private Long pages;
	
	
}
