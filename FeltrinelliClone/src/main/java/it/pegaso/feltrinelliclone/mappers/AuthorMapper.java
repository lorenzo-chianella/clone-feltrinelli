package it.pegaso.feltrinelliclone.mappers;

import org.mapstruct.Mapper;

import it.pegaso.feltrinelliclone.dtos.AuthorDto;
import it.pegaso.feltrinelliclone.models.Author;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

	public abstract AuthorDto toDto(Author source);
	
	public abstract Author toEntity(AuthorDto dto);

}
