package it.pegaso.feltrinelliclone.mappers;

import org.mapstruct.Mapper;

import it.pegaso.feltrinelliclone.dtos.ItemDto;
import it.pegaso.feltrinelliclone.models.Item;

@Mapper(componentModel = "spring")
public interface ItemMapper {
	
	public abstract ItemDto toDto(Item item);
	
	public abstract Item toEntity(ItemDto dto);

}
