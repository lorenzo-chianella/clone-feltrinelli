package it.pegaso.feltrinelliclone.mappers;

import org.mapstruct.Mapper;

import it.pegaso.feltrinelliclone.dtos.GenreDto;
import it.pegaso.feltrinelliclone.models.Genre;

@Mapper(componentModel = "spring")
public interface GenreMapper {

	public abstract GenreDto toDto(Genre source);
	
	public abstract Genre toEntity(GenreDto dto);
}
