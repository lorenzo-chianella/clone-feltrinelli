package it.pegaso.feltrinelliclone.mappers;

import org.mapstruct.Mapper;

import it.pegaso.feltrinelliclone.dtos.CountryDto;
import it.pegaso.feltrinelliclone.models.Country;

@Mapper(componentModel = "spring")
public interface CountryMapper {

	public abstract CountryDto toDto(Country source);

	public abstract Country toEntity(CountryDto dto);
}
