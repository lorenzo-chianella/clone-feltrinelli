package it.pegaso.feltrinelliclone.fileextractor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.util.ResourceUtils;


public class ReadFromFile{

	public static String[] extract(String fileName) {
		String[] fileArray = {};
		try {
			File file = ResourceUtils.getFile(fileName);
			byte[] fileData = Files.readAllBytes(file.toPath());
			String fileContent = new String(fileData);
			fileArray = fileContent.split("\r\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileArray;			
	}
}
