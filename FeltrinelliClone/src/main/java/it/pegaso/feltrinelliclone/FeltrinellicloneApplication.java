package it.pegaso.feltrinelliclone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeltrinellicloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeltrinellicloneApplication.class, args);
	}

}
