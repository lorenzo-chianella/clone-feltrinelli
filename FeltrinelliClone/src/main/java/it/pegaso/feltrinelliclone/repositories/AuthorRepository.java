package it.pegaso.feltrinelliclone.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Author;
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{

	public List<Author> findByCountryId(Long id, Sort sort);
}
