package it.pegaso.feltrinelliclone.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Item;
@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{

	public List<Item> findItemByCdNotNull(Sort sort);
	public List<Item> findItemByBookNotNull(Sort sort);
	public List<Item> findItemByMovieNotNull(Sort sort);
	
	public List<Item> findItemByAuthorId(Long id, Sort sort);
	public List<Item> findItemByCountryId(Long id, Sort sort);
	public List<Item> findByGenreId(Long id, Sort sort);
	public List<Item> findItemByIsVisibleTrue(Sort sort);
	
}
