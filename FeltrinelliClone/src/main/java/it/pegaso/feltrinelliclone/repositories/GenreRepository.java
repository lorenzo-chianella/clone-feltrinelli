package it.pegaso.feltrinelliclone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Genre;
@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{

}
