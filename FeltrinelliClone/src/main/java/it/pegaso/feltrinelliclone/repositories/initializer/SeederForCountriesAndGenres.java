package it.pegaso.feltrinelliclone.repositories.initializer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.pegaso.feltrinelliclone.fileextractor.ReadFromFile;
import it.pegaso.feltrinelliclone.models.Country;
import it.pegaso.feltrinelliclone.models.Genre;
import it.pegaso.feltrinelliclone.repositories.CountryRepository;
import it.pegaso.feltrinelliclone.repositories.GenreRepository;

@Component
public class SeederForCountriesAndGenres {

	private final static String COUNTRIES = "classpath:countries.txt";
	private final static String GENRES = "classpath:genres.txt";

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private GenreRepository genreRepository;
	
	@PostConstruct
	public void seed() {
		if (countryRepository.findAll().size() < 1) {
			List<Country> countries = getCountriesFromFile();
			countryRepository.saveAll(countries);
		}
		if (genreRepository.findAll().size() < 1) {
			List<Genre> genres = getGenresFromFile();
			genreRepository.saveAll(genres);
		}
	}

	public List<Country> getCountriesFromFile() {

		List<Country> countries = new ArrayList<Country>();
		String[] rows = ReadFromFile.extract(COUNTRIES);
		for (String string : rows) {
			countries.add(new Country(string));
		}
		return countries;
	}

	public List<Genre> getGenresFromFile() {

		List<Genre> genres = new ArrayList<Genre>();
		String[] rows = ReadFromFile.extract(GENRES);
		for (String string : rows) {
			genres.add(new Genre(string));
		}
		return genres;
	}

}
