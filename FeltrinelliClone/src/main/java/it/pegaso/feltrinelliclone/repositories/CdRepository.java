package it.pegaso.feltrinelliclone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Cd;
@Repository
public interface CdRepository extends JpaRepository<Cd, Long>{

}
