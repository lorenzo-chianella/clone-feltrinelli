package it.pegaso.feltrinelliclone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Book;
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
