package it.pegaso.feltrinelliclone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.feltrinelliclone.models.Country;
@Repository
public interface CountryRepository extends JpaRepository<Country, Long>{

}
