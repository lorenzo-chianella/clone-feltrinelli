package it.pegaso.feltrinelliclone.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class GenreNotFoundException extends ResponseStatusException{

	public GenreNotFoundException(HttpStatus status, String reason) {
		super(status, reason);
		// TODO Auto-generated constructor stub
	}

	
}
