package it.pegaso.feltrinelliclone.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ItemNotFoundException extends ResponseStatusException {

	public ItemNotFoundException(HttpStatus status, String message) {
		super(status, message);
		// TODO Auto-generated constructor stub
	}

	
}
