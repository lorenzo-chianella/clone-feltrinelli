package it.pegaso.feltrinelliclone.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthorNotFoundException extends ResponseStatusException{

	public AuthorNotFoundException(HttpStatus status, String message) {
		super(status, message);
		// TODO Auto-generated constructor stub
	}
}
