package it.pegaso.feltrinelliclone.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CountryNotFoundException extends ResponseStatusException{

	public CountryNotFoundException(HttpStatus status, String reason) {
		super(status, reason);
		// TODO Auto-generated constructor stub
	}
}
