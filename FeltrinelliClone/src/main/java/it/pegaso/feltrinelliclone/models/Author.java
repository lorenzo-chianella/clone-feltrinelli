package it.pegaso.feltrinelliclone.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	@NotNull
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private Long yearOfBirth;
	@NotNull
	private String birthPlace;
	
	@ManyToOne
	private Country country;
	
	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
	private List<Item> items;
}
