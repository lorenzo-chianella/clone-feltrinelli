package it.pegaso.feltrinelliclone.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class Cd {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( updatable = false)
	@NotNull
	private Long id;
	private Long tracks;
	private Long duration;
	@OneToOne(mappedBy = "cd", cascade = CascadeType.PERSIST)
	private Item item;
}
