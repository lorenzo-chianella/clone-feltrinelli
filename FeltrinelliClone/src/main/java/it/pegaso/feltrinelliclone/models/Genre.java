package it.pegaso.feltrinelliclone.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Genre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Column(updatable = false)
	private Long id;
	@NotNull 
	private String description;
	@OneToMany(mappedBy = "genre")
	private List<Item> items;
	
	public Genre(@NotNull String description) {
		this.description = description;
	}
}
