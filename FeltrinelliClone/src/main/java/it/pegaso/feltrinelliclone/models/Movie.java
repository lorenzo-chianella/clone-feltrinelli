package it.pegaso.feltrinelliclone.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import it.pegaso.feltrinelliclone.enumerators.SupportType;
import lombok.Data;

@Entity
@Data
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;
	private String synopsis;
	private SupportType support;
	private Long duration;
	@OneToOne(mappedBy = "movie", cascade = CascadeType.PERSIST)
	private Item item;
}
