package it.pegaso.feltrinelliclone.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	@NotNull
	private Long id;
	@NotNull
	private String name;
	
	@OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
	private List<Author> authors;
	
	@OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
	private List<Item> items;

	public Country(@NotNull String name) {
		this.name = name;
	}
	
	
}
