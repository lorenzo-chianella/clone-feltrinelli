package it.pegaso.feltrinelliclone.models;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@NotNull
	@Column(updatable = false)
	private Long id;
	
	@NotNull
	private String title;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateOfRelease;
	
	@NotNull
	private String publisher;
	@NotNull
	private BigDecimal price;
	@NotNull
	private String ean;
	@NotNull
	private Long availability;
	@NotNull
	private Boolean isVisible;
	
	private String imageUrl;
	@ManyToOne
	private Author author;
	@ManyToOne
	private Country country;
	@ManyToOne
	private Genre genre;
	@OneToOne(cascade = CascadeType.ALL)
	private Book book;
	@OneToOne(cascade = CascadeType.ALL)
	private Movie movie;
	@OneToOne(cascade = CascadeType.ALL)
	private Cd cd;
	
}
