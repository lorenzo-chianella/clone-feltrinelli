package it.pegaso.feltrinelliclone.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.feltrinelliclone.dtos.CountryDto;
import it.pegaso.feltrinelliclone.services.CountryService;

@RestController
@RequestMapping("/api/v1/country")
public class CountryController {

	@Autowired
	CountryService countryService;
	
	@GetMapping
	public ResponseEntity<List<CountryDto>> getAllCountries(){
		List<CountryDto> countries = countryService.getAll();
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CountryDto> getById(@PathVariable("id") Long id){
		CountryDto country = countryService.findById(id);
		return new ResponseEntity<>(country, HttpStatus.OK);
	}
}
