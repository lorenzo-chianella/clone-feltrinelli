package it.pegaso.feltrinelliclone.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.feltrinelliclone.dtos.GenreDto;
import it.pegaso.feltrinelliclone.services.GenreService;

@RestController
@RequestMapping("/api/v1/genre")
public class GenreController {

	@Autowired
	GenreService genreService;
	
	@GetMapping
	public ResponseEntity<List<GenreDto>> getAllGenres(){
		List<GenreDto> genres = genreService.getAll();
		return new ResponseEntity<>(genres, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<GenreDto> getById(@PathVariable("id") Long id){
		GenreDto genre = genreService.findById(id);
		return new ResponseEntity<>(genre, HttpStatus.OK);
	}
}
