package it.pegaso.feltrinelliclone.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.feltrinelliclone.dtos.AuthorDto;
import it.pegaso.feltrinelliclone.dtos.ItemDto;
import it.pegaso.feltrinelliclone.services.AuthorService;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {

	@Autowired
	AuthorService authorService;
	
	@GetMapping
	public ResponseEntity<List<AuthorDto>> getAllAuthors(){
		List<AuthorDto> authors = authorService.getAll();
		return new ResponseEntity<>(authors, HttpStatus.OK);
	}
	
	@GetMapping("author/{id}")
	public ResponseEntity<AuthorDto> getById(@PathVariable("id") Long id){
		AuthorDto author = authorService.findById(id);
		return new ResponseEntity<>(author, HttpStatus.OK);
	}
	
	@PostMapping("/save")
	@Transactional
	public ResponseEntity<AuthorDto> save(@RequestBody AuthorDto a){
		AuthorDto author = authorService.save(a);
		return new ResponseEntity<>(author, HttpStatus.OK);
	}
	
	@PutMapping("/update")
	@Transactional
	public ResponseEntity<AuthorDto> update(@RequestBody AuthorDto i){
		AuthorDto author = authorService.update(i);
		return new ResponseEntity<>(author, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@Transactional
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String result = authorService.delete(id);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
