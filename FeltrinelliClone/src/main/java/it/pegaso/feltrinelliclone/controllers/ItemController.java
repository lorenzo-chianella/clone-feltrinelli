package it.pegaso.feltrinelliclone.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.feltrinelliclone.dtos.ItemDto;
import it.pegaso.feltrinelliclone.services.ItemService;

@RestController
@RequestMapping("/api/v1")
public class ItemController {

	@Autowired
	ItemService itemService;
	
	@GetMapping("/items")
	public ResponseEntity<List<ItemDto>> getAllItems(){
		List<ItemDto> items = itemService.getAll();
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/cds")
	public ResponseEntity<List<ItemDto>> getAllCds(){
		List<ItemDto> items = itemService.getAllCds();
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/books")
	public ResponseEntity<List<ItemDto>> getAllBooks(){
		List<ItemDto> items = itemService.getAllBooks();
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/movies")
	public ResponseEntity<List<ItemDto>> getAllMovies(){
		List<ItemDto> items = itemService.getAllMovies();
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("item/{id}")
	public ResponseEntity<ItemDto> getById(@PathVariable("id") Long id){
		ItemDto item = itemService.findById(id);
		return new ResponseEntity<>(item, HttpStatus.OK);
	}
	
	@GetMapping("/items/author/{id}")
	public ResponseEntity<List<ItemDto>> getByAuthorId(@PathVariable("id") Long id){
		List<ItemDto> items = itemService.findItemByAuthorId(id);
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/items/genre/{id}")
	public ResponseEntity<List<ItemDto>> getByGenreId(@PathVariable("id") Long id){
		List<ItemDto> items = itemService.findItemByGenreId(id);
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/items/country/{id}")
	public ResponseEntity<List<ItemDto>> getByCountryId(@PathVariable("id") Long id){
		List<ItemDto> items = itemService.findItemByCountryId(id);
		return new ResponseEntity<>(items, HttpStatus.OK);
	}
	
	@GetMapping("/items/v")
	public ResponseEntity<List<ItemDto>> getAllVisible(){
		List<ItemDto> items = itemService.findItemByIsVisibleTrue();
		return new ResponseEntity<>(items, HttpStatus.OK);
	}

	@PostMapping("/item/save")
	@Transactional
	public ResponseEntity<ItemDto> save(@RequestBody ItemDto i){
		ItemDto item = itemService.save(i);
		return new ResponseEntity<>(item, HttpStatus.OK);
	}
	
	@PutMapping("/item/update")
	@Transactional
	public ResponseEntity<ItemDto> update(@RequestBody ItemDto i){
		ItemDto item = itemService.update(i);
		return new ResponseEntity<>(item, HttpStatus.OK);
	}
	
	@DeleteMapping("/item/delete")
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String result = itemService.delete(id);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
