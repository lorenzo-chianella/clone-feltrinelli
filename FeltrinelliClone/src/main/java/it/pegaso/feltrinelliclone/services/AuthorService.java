package it.pegaso.feltrinelliclone.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.pegaso.feltrinelliclone.dtos.AuthorDto;
import it.pegaso.feltrinelliclone.exceptions.AuthorNotFoundException;
import it.pegaso.feltrinelliclone.exceptions.ItemNotFoundException;
import it.pegaso.feltrinelliclone.mappers.AuthorMapper;
import it.pegaso.feltrinelliclone.models.Author;
import it.pegaso.feltrinelliclone.repositories.AuthorRepository;

@Service
public class AuthorService {

	@Autowired
	AuthorRepository authorRepository;

	@Autowired
	AuthorMapper authorMapper;

	public List<AuthorDto> getAll() {
		return authorRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(authorMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public AuthorDto findById(Long id) {
		Author result = authorRepository.findById(id)
				.orElseThrow(() -> new AuthorNotFoundException(
						HttpStatus.NOT_FOUND, "Autore non trovato"));
		return authorMapper.toDto(result);
		
	}
	
	public List<AuthorDto> findByCountryId(Long id) {
		List<AuthorDto> result = authorRepository.findByCountryId(id, Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(authorMapper::toDto)
				.collect(Collectors.toList());
		return result;
		
	}
	@Transactional
	public AuthorDto save(AuthorDto dto) {
		Author entity = authorMapper.toEntity(dto);
		return authorMapper.toDto(authorRepository.save(entity));
	}
	@Transactional
	public AuthorDto update(AuthorDto dto) {
		if(!authorRepository.findById(dto.getId()).isPresent()) {
			throw new AuthorNotFoundException(HttpStatus.NOT_FOUND, "Autore non trovato");
		}
		Author entity = authorMapper.toEntity(dto);
		AuthorDto update = authorMapper.toDto(authorRepository.save(entity));
		return update;
	}
	
	public String delete(Long id) {
		if(!authorRepository.findById(id).isPresent()) {
			throw new ItemNotFoundException(HttpStatus.NOT_FOUND, "Articolo non trovato");
		}
		authorRepository.deleteById(id);
		return "Deleted Successfully";
	}
}
