package it.pegaso.feltrinelliclone.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.feltrinelliclone.dtos.CountryDto;
import it.pegaso.feltrinelliclone.exceptions.CountryNotFoundException;
import it.pegaso.feltrinelliclone.mappers.CountryMapper;
import it.pegaso.feltrinelliclone.models.Country;
import it.pegaso.feltrinelliclone.repositories.CountryRepository;

@Service
public class CountryService {

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	CountryMapper countryMapper;
	
	public List<CountryDto> getAll() {
		return countryRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(countryMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public CountryDto findById(Long id) {
		Country result = countryRepository.findById(id)
				.orElseThrow(() -> new CountryNotFoundException(
						HttpStatus.NOT_FOUND, "Genere non trovato"));
		return countryMapper.toDto(result);	
	}
}
