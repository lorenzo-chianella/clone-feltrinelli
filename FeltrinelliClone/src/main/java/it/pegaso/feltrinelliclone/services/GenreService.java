package it.pegaso.feltrinelliclone.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.pegaso.feltrinelliclone.dtos.GenreDto;
import it.pegaso.feltrinelliclone.exceptions.AuthorNotFoundException;
import it.pegaso.feltrinelliclone.exceptions.GenreNotFoundException;
import it.pegaso.feltrinelliclone.exceptions.ItemNotFoundException;
import it.pegaso.feltrinelliclone.mappers.GenreMapper;
import it.pegaso.feltrinelliclone.models.Genre;
import it.pegaso.feltrinelliclone.repositories.GenreRepository;

@Service
public class GenreService {

	@Autowired
	GenreRepository genreRepository;

	@Autowired
	GenreMapper genreMapper;
	
	public List<GenreDto> getAll() {
		return genreRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(genreMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public GenreDto findById(Long id) {
		Genre result = genreRepository.findById(id)
				.orElseThrow(() -> new GenreNotFoundException(
						HttpStatus.NOT_FOUND, "Genere non trovato"));
		return genreMapper.toDto(result);	
	}
	
//	@Transactional
//	public GenreDto save(GenreDto dto) {
//		Genre entity = genreMapper.toEntity(dto);
//		return genreMapper.toDto(genreRepository.save(entity));
//	}
//	
//	@Transactional
//	public GenreDto update(GenreDto dto) {
//		if(!genreRepository.findById(dto.getId()).isPresent()) {
//			throw new AuthorNotFoundException(HttpStatus.NOT_FOUND, "Autore non trovato");
//		}
//		Genre entity = genreMapper.toEntity(dto);
//		GenreDto update = genreMapper.toDto(genreRepository.save(entity));
//		return update;
//	}
//	
//	public String delete(Long id) {
//		if(!genreRepository.findById(id).isPresent()) {
//			throw new ItemNotFoundException(HttpStatus.NOT_FOUND, "Articolo non trovato");
//		}
//		genreRepository.deleteById(id);
//		return "Deleted Successfully";
//	}
}
