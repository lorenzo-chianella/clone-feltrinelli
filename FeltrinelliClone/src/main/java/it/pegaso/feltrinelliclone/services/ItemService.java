package it.pegaso.feltrinelliclone.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.pegaso.feltrinelliclone.dtos.ItemDto;
import it.pegaso.feltrinelliclone.exceptions.ItemNotFoundException;
import it.pegaso.feltrinelliclone.mappers.ItemMapper;
import it.pegaso.feltrinelliclone.models.Item;
import it.pegaso.feltrinelliclone.repositories.ItemRepository;

@Service
public class ItemService {

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	ItemMapper itemMapper;

	public List<ItemDto> getAll() {
		return itemRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}

	public List<ItemDto> getAllCds() {
		return itemRepository.findItemByCdNotNull(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ItemDto> getAllBooks() {
		return itemRepository.findItemByBookNotNull(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ItemDto> getAllMovies() {
		return itemRepository.findItemByMovieNotNull(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public ItemDto findById(Long id) {
		Item result = itemRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(
						HttpStatus.NOT_FOUND, "Articolo non trovato"));
		return itemMapper.toDto(result);
	}
	
	public List<ItemDto> findItemByAuthorId(Long authorId){
		return itemRepository.findItemByAuthorId(authorId, Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ItemDto> findItemByCountryId(Long countryId){
		return itemRepository.findItemByCountryId(countryId, Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ItemDto> findItemByGenreId(Long genreId){
		return itemRepository.findItemByCountryId(genreId, Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	public List<ItemDto> findItemByIsVisibleTrue(){
		return itemRepository.findItemByIsVisibleTrue(Sort.by(Sort.Direction.ASC, "id"))
				.stream()
				.map(itemMapper::toDto)
				.collect(Collectors.toList());
	}
	
	@Transactional
	public ItemDto save(ItemDto dto) {
		Item entity = itemMapper.toEntity(dto);
		return itemMapper.toDto(itemRepository.save(entity));
	}
	
	@Transactional
	public ItemDto update(ItemDto dto) {
		if(!itemRepository.findById(dto.getId()).isPresent()) {
			throw new ItemNotFoundException(HttpStatus.NOT_FOUND, "Articolo non trovato");
		}
		Item entity = itemMapper.toEntity(dto);
		ItemDto update = itemMapper.toDto(itemRepository.save(entity));
		return update;
	}
	
	public String delete(Long id) {
		if(!itemRepository.findById(id).isPresent()) {
			throw new ItemNotFoundException(HttpStatus.NOT_FOUND, "Articolo non trovato");
		}
		itemRepository.deleteById(id);
		return "Deleted Successfully";
	}
}
